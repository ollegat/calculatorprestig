const path = require("path");
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const { FileListPlugin } = require('./filelistplugin.js');

module.exports = {
  entry: {
      app: './src/index.tsx'
  },
  output: {
      filename: 'bundle-[hash].js',
      path: path.resolve(__dirname, 'dist')
    },
  devtool: 'source-map',
  resolve: {
    extensions: ['.ts', '.tsx', '.js', '.css', '.scss'],
    alias: {
      'entities': path.resolve(__dirname, 'src/entities'),
      'api': path.resolve(__dirname, 'src/api'),
      'uikit': path.resolve(__dirname, 'src/uikit')
  }            
  },
  devServer: {
    static: {
      directory: path.join(__dirname, "dist"),
    },
    compress: true,
    port: 9000,
    open: true,
    client: {
      progress: true,
    },
  }, 
  module: {
    rules: [
      {
        test: /\.(ts)x?$/,
        exclude: /node_modules/,
        use: [
          { loader: 'babel-loader'}
        ],
      },
      {
        test: /\.scss$/i,
        include: path.join(__dirname, 'src'),
        use: [
          MiniCssExtractPlugin.loader,
          { loader: "css-modules-typescript-loader"},  
          { loader: "css-loader", options: { modules: true } }, 
          { 
            loader: "sass-loader",
          }              
        ]
      },
      {
        test: /\.(png|svg|jpg|gif)$/,
        use: ["file-loader"]
      },
    ]
  },
  plugins: [
    new CleanWebpackPlugin(),
    new HtmlWebpackPlugin({
      filename: '../dist/index.html',
      template: 'src/index.html'
    }),
    new MiniCssExtractPlugin({
      filename: "style-[hash].css",
    }),    
    new FileListPlugin({
      outputFile: 'manifest.json',
    }),    
  ]
}
